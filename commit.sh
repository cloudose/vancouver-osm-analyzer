#!/bin/bash

REPO=repo

cd "$REPO"
git fetch
git reset --hard origin/master
mkdir -p "lists/buses"
mkdir -p "lists/hydrants"
cd ../src
node downloader.js
node osm_bus_stops.js
node translink_bus_stops.js
node osm_fire_hydrants.js
cd ..
GIT_AUTHOR_DATE="`cat TIMESTAMP`"
GIT_COMMITTER_DATE="$GIT_AUTHOR_DATE"
cd "$REPO"
git add lists/buses/*
git add lists/hydrants/*
git commit -m "Update lists: $GIT_AUTHOR_DATE" --date="$GIT_AUTHOR_DATE"
git push origin HEAD:master
