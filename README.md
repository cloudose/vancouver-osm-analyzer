# Vancouver OpenStreetMap Analyzer
This project is used to watch changes in [OpenStreetMap](https://www.openstreetmap.org). Its current focus is the Greater Vancouver, BC area in Canada.

The output of this repository can be found here: https://github.com/jaller94/vancouver-osm-analysis-data/

## Monitored elements
  * Translink bus stops
  * fire hydrants

## How to use
1. Create a ./repo folder.
2. Initialize a Git repository with `git init`.
3. Make sure it has a default remote location to push to. You could push it to GitHub or GitLab.
4. Download the most up-to-date table of [Translink’s bus stops](https://developer.translink.ca/ServicesGtfs/GtfsData). Unzip the package and put `stops.txt` into a new ./data folder.
5. Run `bash commit.sh` to create or update the report.
6. (optional) Automate the previous step as a reoccurring job using cron.
