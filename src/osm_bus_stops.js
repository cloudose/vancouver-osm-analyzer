'use strict';
const fs = require('fs').promises;
const path = require('path');

const DatasetLoader = require('./dataset-loader.js');
const count = require('./helper/count.js');

const repoDir = '../repo';

const {
    includesBusStopNumber,
    isBusStopNumber,
    parseBusStopNumber
} = require('./helper/bus_stops.js');


function addStop(stops, id, element) {
    if (id === undefined) return;
    if (stops[id] === undefined) {
        stops[id] = [];
    }
    stops[id].push(element);
}

function isTranslink(operator) {
    operator = operator.toLowerCase();
    return (
        operator.includes('translink')
        || operator.includes('blue bus')
        || operator.includes('coast mountain bus')
    );
}

function linkBusStop(stop) {
    let str = `[Node ${stop.id}](https://www.openstreetmap.org/node/${stop.id})`;
    if (stop.tags.name) str += ` (${stop.tags.name})`;
    return str;
}

async function main() {
    const root = await DatasetLoader('bus');

    const correct_stops = [];
    const incorrect_stops = [];
    const missing_ref = [];
    const different_operator = [];
    const different_schema = [];
    const all_stops = [];

    for (const element of root.elements) {
        const tags = element.tags;
        if (!tags) {
            console.warn(element);
            continue;
        }
        if (tags.operator && !isTranslink(tags.operator)) {
            addStop(different_operator, tags.operator, element);
        } else if (isBusStopNumber(tags.ref)) {
            const id = parseBusStopNumber(tags.ref.match(/^\d{5}$/)[0]);
            addStop(correct_stops, id, element);
            addStop(all_stops, id, element);
        } else if (tags.ref) {
            addStop(different_schema, tags.ref, element);
        } else if (includesBusStopNumber(tags.name)) {
            const id = parseBusStopNumber(tags.name);
            addStop(incorrect_stops, id, element);
            addStop(all_stops, id, element);
        } else if (includesBusStopNumber(tags['name:en'])) {
            const id = parseBusStopNumber(tags['name:en']);
            addStop(incorrect_stops, id, element);
            addStop(all_stops, id, element);
        } else if (includesBusStopNumber(tags['name:fr'])) {
            const id = parseBusStopNumber(tags['name:fr']);
            addStop(incorrect_stops, id, element);
            addStop(all_stops, id, element);
        } else {
            missing_ref.push(element);
        }
    }

    const copyright = 'OpenStreetMap® is open data, licensed under the Open Data Commons Open Database License (ODbL) by the OpenStreetMap Foundation (OSMF).\n';

    let data;
    data = `---
layout: page
title: "TransLink area: Bus stops mapping issues"
permalink: buses/incorrect.html
categories: bus_stops
---\n`;
    data += copyright;
    data += '\n## Duplicate bus stops numbers\n';
    for (const stop_id in all_stops) {
        const stops = all_stops[stop_id];
        if (stops && stops.length > 1) {
            data += '* ' + stop_id + '\n';
            for(const stop of stops) {
                data += '  * ' + linkBusStop(stop) + '\n';
            }
        }
    }
    data += '\n## Incorrectly mapped bus stop numbers (' + count(incorrect_stops) + ')\n';
    data += `These bus stops have an identifiable 5-digit bus stop number. However, the number is mapped in \`name\` or \`name:en\` but not \`ref\`.\n`;
    for (const stop_id in all_stops) {
        const stops = incorrect_stops[stop_id];
        if (stops) {
            for(const stop of stops) {
                data += `  * ${stop_id} – ${linkBusStop(stop)}\n`;
            }
        }
    }
    fs.writeFile(path.join(repoDir, 'lists/buses/incorrect.md'), data);

    data = `---
layout: page
title: "TransLink area: Identifiable bus stops"
permalink: buses/identifiable.html
categories: bus_stops
---\n`;
    data += copyright;
    data += `\n## Identifiable bus stops (${count(all_stops)})\n`;
    data += `These bus stops contain a 5-digit number in their \`ref\`, \`name\` or \`name:en\` tag.\n`;
    for (const stop_id in all_stops) {
        const stops = all_stops[stop_id];
        if (stops) {
            for(const stop of stops) {
                data += `  * ${stop_id} – ${linkBusStop(stop)}\n`;
            }
        }
    }
    await fs.writeFile(path.join(repoDir, 'lists/buses/identifiable.md'), data);

    data = `---
layout: page
title: "TransLink area: Unidentifiable bus stops"
permalink: buses/unidentifiable.html
categories: bus_stops
---\n`;
    data += copyright;
    data += `\n## Different operator (${count(different_operator)})\n`;
    data += `These bus stops have an \`operator\` tag, however, it's value is not Translink.\n`;
    for (const stop_id in different_operator) {
        const stops = different_operator[stop_id];
        if (stops) {
            for(const stop of stops) {
                data += `* \`${stop_id}\` – ${linkBusStop(stop)}\n`;
            }
        }
    }
    data += `\n## Different ref schema (${count(different_schema)})\n`;
    data += `These bus stops have a \`ref\` tag, however, it differs from TransLink's five digit standard.\n`;
    for (const stop_id in different_schema) {
        const stops = different_schema[stop_id];
        if (stops) {
            for(const stop of stops) {
                data += `* \`${stop_id}\` – ${linkBusStop(stop)}\n`;
            }
        }
    }
    data += `\n## Bus stop number missing (${count(missing_ref)})\n`;
    data += `These bus stops don't contain a 5-digit number in their \`ref\`, \`name\`, \`name:en\` or \`name:fr\` tag. Please add a \`ref\` tag if you know their bus stop number.\n`;
    for (const stop of missing_ref) {
        data += `* ${linkBusStop(stop)}\n`;
    }
    await fs.writeFile(path.join(repoDir, 'lists/buses/unidentifiable.md'), data);

    await fs.writeFile('../TIMESTAMP', root.osm3s.timestamp_osm_base);
}

main().catch((error) => {
    console.error(error);
    process.exit(1);
});
