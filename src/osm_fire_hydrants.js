'use strict';
const fs = require('fs');
const path = require('path');

const input = fs.readFileSync('../data/fire_hydrant.json', 'utf8');
const root = JSON.parse(input);

const count = require('./helper/count.js');

const repoDir = '../repo';

function addStop(stops, id, element) {
    if (id === undefined) return;
    if (stops[id] === undefined) {
        stops[id] = [];
    }
    stops[id].push(element);
}

const correct_hydrants = {};
const missing_ref = [];
const different_schema = {};
const all_hydrants = {};

const fireHydrantRefRegEx = /^[E-W][0-2]\d{4}$/;

for (const element of root.elements) {
    const tags = element.tags;
    if (fireHydrantRefRegEx.test(tags.ref)) {
        const id = tags.ref.match(fireHydrantRefRegEx)[0];
        addStop(correct_hydrants, id, element);
        addStop(all_hydrants, id, element);
    } else if (tags.ref) {
        addStop(different_schema, tags.ref, element);
    } else {
        missing_ref.push(element);
    }
}

function linkFireHydrant(node) {
    return `[Node ${node.id}](https://www.openstreetmap.org/node/${node.id})`;
}

const copyright = 'OpenStreetMap® is open data, licensed under the Open Data Commons Open Database License (ODbL) by the OpenStreetMap Foundation (OSMF).\n';

let data;
data = `---
layout: page
title: "Vancouver: Fire hydrant mapping issues"
permalink: hydrants/incorrect.html
categories: fire_hydrants
---\n`;
data += copyright;
data += '\n## Duplicate fire hydrant codes\n';
for (const hydrant_id in all_hydrants) {
    const hydrants = all_hydrants[hydrant_id];
    if (hydrants && hydrants.length > 1) {
        data += '* ' + hydrant_id + '\n';
        for(const hydrant of hydrants) {
            data += '  * ' + linkFireHydrant(hydrant) + '\n';
        }
    }
}
fs.writeFileSync(path.join(repoDir, 'lists/hydrants/incorrect.md'), data);

data = `---
layout: page
title: "Vancouver: Unidentifiable fire hydrants"
permalink: hydrants/unidentifiable.html
categories: fire_hydrants
---\n`;
data += copyright;
data += '\n## Different ref schema (' + count(different_schema) + ')\n';
data += `These fire hydrants have a \`ref\` tag, however, it differs from the region identifier and a 5-digit standard of Vancouver. Fire hydrants in Burnaby have a 4-digit number and are still part of this list.\n`;
for (const hydrant_id in different_schema) {
    const hydrants = different_schema[hydrant_id];
    if (hydrants) {
        for(const hydrant of hydrants) {
            data += `* \`${hydrant_id}\` – ` + linkFireHydrant(hydrant) + '\n';
        }
    }
}
data += '\n## Fire hydrants without a code (' + count(missing_ref) + ')\n';
data += `These fire hydrants don't contain a region identifier and a 5-digit number in their \`ref\` tag. Please add a \`ref\` tag, if you know their fire hydrant number.\n`;
for (const hydrant of missing_ref) {
    data += '* ' + linkFireHydrant(hydrant) + '\n';
}
fs.writeFileSync(path.join(repoDir, 'lists/hydrants/unidentifiable.md'), data);

data = `---
layout: page
title: "Vancouver: Identifiable fire hydrants"
permalink: hydrants/identifiable.html
categories: fire_hydrants
---\n`;
data += copyright;
data += `\n## Identifiable fire hydrants (${count(all_hydrants)})\n`;
data += `These fire hydrant's \`ref\` is a region identifier and a 5-digit number.\n`;
Object.keys(correct_hydrants).sort().forEach((hydrant_id) => {
    const hydrants = all_hydrants[hydrant_id];
    if (hydrants) {
        for(const hydrant of hydrants) {
            const hydrantLink = linkFireHydrant(hydrant);
            data += `* ${hydrant_id} – ${hydrantLink}\n`;
        }
    }
});
fs.writeFileSync(path.join(repoDir, 'lists/hydrants/identifiable.md'), data);
