const BBox = require('./bbox.class.js');
const OverpassQuery = require('./overpass-query.class.js');

describe('toString', () => {
    it('matches snapshot', () => {
        const bbox = new BBox(-0.489, 51.28, 0.236, 51.686);
        const query = new OverpassQuery();
        query.addWish('node', 'emergency', '*', bbox);
        expect(query.toString()).toMatchSnapshot();
    });
});
