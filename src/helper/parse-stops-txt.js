'use strict';

const parseStopsTxt = (data) => {
    const lines = data.split(/(?:\r\n|\r|\n)/g).slice(1);
    return lines.map((line) => {
        const match = line.match(/^(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?),(.*?)$/);
        if (match) {
            return {
                id: match[4],
                code: match[2],
                name: match[8],
                desc: match[7],
                lat: match[1],
                lon: match[3],
                zone_id: match[10],
                url: match[5],
                location_type: match[9],
                parent_station: match[6],
            };
        }
    });
}

module.exports = parseStopsTxt;
