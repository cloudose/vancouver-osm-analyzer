'use strict';
class OverpassQuery {
    constructor(output = 'json', timeout = 25) {
        this.output = output;
        this.timeout = timeout;
        this.wishes = [];
    }

    addWish(type, tag, value, bbox) {
        this.wishes.push({
            type,
            tag,
            value,
            bbox,
        });
    }

    toString() {
        let str = `[out:${this.output}][timeout:${this.timeout}];\n`;
        str += `(\n`;
        for(const wish of this.wishes) {
            str += `  ${wish.type}["${wish.tag}"="${wish.value}"]`;
            str += `(${wish.bbox.toOverpassString()});\n`;
        }
        str += `);\n`;
        str += `out body;\n>;\nout skel qt;`
        return str;
    }
}

module.exports = OverpassQuery;
