'use strict';
class BBox {
    constructor(left, bottom, right, top) {
        this.left = Number.parseFloat(left);
        this.bottom = Number.parseFloat(bottom);
        this.right = Number.parseFloat(right);
        this.top = Number.parseFloat(top);
    }

    static fromNominatimArray(array) {
        const [
            bottom,
            top,
            left,
            right,
        ] = array;
        return new BBox(left, bottom, right, top);
    }

    static fromOverpassString(string) {
        const [
            bottom,
            left,
            top,
            right,
        ] = string.split(',');
        return new BBox(left, bottom, right, top);
    }

    static fromPoints(points) {
        let min_lat = 90;
        let max_lat = -90;
        let min_lon = 180;
        let max_lon = -180;
        points.forEach((point) => {
            min_lat = Math.min(min_lat, point.lat);
            max_lat = Math.max(max_lat, point.lat);
            min_lon = Math.min(min_lon, point.lon);
            max_lon = Math.max(max_lon, point.lon);
        });
        return new BBox(min_lon, min_lat, max_lon, max_lat);
    }

    toNominatimArray() {
        return [
            String(this.bottom),
            String(this.top),
            String(this.left),
            String(this.right),
        ];
    }

    toOverpassString() {
        return `${this.bottom},${this.left},${this.top},${this.right}`;
    }

    toString() {
        return `(bbox=${this.left},${this.bottom},${this.right},${this.top})`;
    }
}

module.exports = BBox;
