'use strict';

const count = (array) => {
    let count = 0;
    for(const _ in array) {
        count++;
    }
    return count;
}

module.exports = count;
