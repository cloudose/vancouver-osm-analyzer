const BBox = require('./bbox.class.js');

describe('Constructor', () => {
    it('floats', () => {
        const bbox = new BBox(-0.489, 51.28, 0.236, 51.686);
        expect(bbox.left).toBe(-0.489);
        expect(bbox.bottom).toBe(51.28);
        expect(bbox.right).toBe(0.236);
        expect(bbox.top).toBe(51.686);
    });
    it('strings', () => {
        const bbox = new BBox('-0.489', '51.28', '0.236', '51.686');
        expect(bbox.left).toBe(-0.489);
        expect(bbox.bottom).toBe(51.28);
        expect(bbox.right).toBe(0.236);
        expect(bbox.top).toBe(51.686);
    });
});

describe('Nominatim Array', () => {
    it('fromNominatimArray', () => {
        const boundingbox = ['51.28', '51.686', '-0.489', '0.236'];
        const bbox = BBox.fromNominatimArray(boundingbox);
        expect(bbox.left).toBe(-0.489);
        expect(bbox.bottom).toBe(51.28);
        expect(bbox.right).toBe(0.236);
        expect(bbox.top).toBe(51.686);

    });
    it('toNominatimArray' , () => {
        const expected = ['51.28', '51.686', '-0.489', '0.236'];
        const bbox = new BBox(-0.489, 51.28, 0.236, 51.686);
        expect(bbox.toNominatimArray()).toEqual(expected);
    });
});

describe('Overpass String', () => {
    it('fromOverpassString' , () => {
        const boundingbox = '51.28,-0.489,51.686,0.236';
        const bbox = BBox.fromOverpassString(boundingbox);
        expect(bbox.left).toBe(-0.489);
        expect(bbox.bottom).toBe(51.28);
        expect(bbox.right).toBe(0.236);
        expect(bbox.top).toBe(51.686);
    });
    it('toOverpassString' , () => {
        const expected = '51.28,-0.489,51.686,0.236';
        const bbox = new BBox(-0.489, 51.28, 0.236, 51.686);
        expect(bbox.toOverpassString()).toEqual(expected);
    });
});
