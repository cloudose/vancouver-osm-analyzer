'use strict';
const m = {};

m.includesBusStopNumber = (str) => {
    return /[56]\d{4}/.test(str);
}

m.isBusStopNumber = (str) => {
    return /^[56]\d{4}$/.test(str);
}

m.parseBusStopNumber = (str) => {
    return Number.parseInt(str.match(/[56]\d{4}/)[0]);
}

module.exports = m;
