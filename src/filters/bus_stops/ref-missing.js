'use strict';

const {
    includesBusStopNumber,
} = require('./helper/bus_stops.js');

const filter = (elements) => {
    return elements.filter(({tags}) => {
        if (includesBusStopNumber(tags.ref)) return true;
        if (includesBusStopNumber(tags.name)) return true;
        if (includesBusStopNumber(tags['name:en'])) return true;
        if (includesBusStopNumber(tags['name:fr'])) return true;
        return false;
    });
};

module.exports = filter;
