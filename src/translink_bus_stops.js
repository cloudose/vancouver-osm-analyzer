'use strict';
const fs = require('fs');
const path = require('path');

const osm_input = fs.readFileSync('../data/bus.json', 'utf8');
const osm_root = JSON.parse(osm_input);

const BBox = require('./helper/bbox.class.js');
const count = require('./helper/count.js');
const parseStopsTxt = require('./helper/parse-stops-txt.js');

const repoDir = '../repo';

const {
    includesBusStopNumber,
    isBusStopNumber,
    parseBusStopNumber
} = require('./helper/bus_stops.js');

function addStop(stops, id, element) {
    if (id === undefined) return;
    if (stops[id] === undefined) {
        stops[id] = [];
    }
    stops[id].push(element);
}

const correctStops = {};
const incorrectStops = {};
const missingRef = [];
const differentSchema = {};
const allStops = {};

for (const element of osm_root.elements) {
    const tags = element.tags || {};
    let id;
    if (isBusStopNumber(tags.ref)) {
        id = parseBusStopNumber(tags.ref);
        addStop(correctStops, id, element);
    } else if (includesBusStopNumber(tags.name)) {
        id = parseBusStopNumber(tags.name);
        addStop(incorrectStops, id, element);
    } else if (includesBusStopNumber(tags['name:en'])) {
        id = parseBusStopNumber(tags['name:en']);
        addStop(incorrectStops, id, element);
    } else if (includesBusStopNumber(tags['name:fr'])) {
        id = parseBusStopNumber(tags['name:fr']);
        addStop(incorrectStops, id, element);
    } else {
        missingRef.push(element);
    }
    if (id) {
        addStop(allStops, id, element);
    }
}

const translink_input = fs.readFileSync('../data/stops.txt', 'utf8');
let translink_stops = parseStopsTxt(translink_input);
translink_stops = translink_stops.filter(stop => !!stop);
translink_stops = translink_stops.filter(stop => isBusStopNumber(stop.code));

const osm_exists = [];
const osm_missing = [];
const osm_too_much = [];

function findTransLinkStop(ref) {
    return translink_stops.find((stop) => ref == (stop || {}).code);
}

for(const tl_stop of translink_stops) {
    if (correctStops[tl_stop.code] || incorrectStops[tl_stop.code]) {
        osm_exists[tl_stop.code] = tl_stop;
    } else {
        osm_missing[tl_stop.code] = tl_stop;
    }
}

const bbox = BBox.fromPoints(translink_stops);
console.log('Bbox of TransLink’s bus stops: ' + bbox.toOverpassString());

for (const ref in correctStops) {
    if (!findTransLinkStop(ref)) {
        osm_too_much[ref] = correctStops[ref];
    }
}

for (const ref in incorrectStops) {
    if (!findTransLinkStop(ref)) {
        osm_too_much[ref] = incorrectStops[ref];
    }
}

function linkBusStop(stop) {
    let str = `[Node ${stop.id}](https://www.openstreetmap.org/node/${stop.id})`;
    if (stop.tags.name) str += ` (${stop.tags.name})`;
    return str;
}

const copyright = 'OpenStreetMap® is open data, licensed under the Open Data Commons Open Database License (ODbL) by the OpenStreetMap Foundation (OSMF).';

let data = '';
data = `---
layout: page
title: "TransLink area: Comparison with TransLink Data"
permalink: buses/compare_with_translink.html
categories: bus_stops
---
This page got generated using data from OpenStreetMap and the [TransLink Open API](https://developer.translink.ca/ServicesGtfs/GtfsData).

${copyright}

Route and arrival data used in this product or service is provided by permission of TransLink. TransLink assumes no responsibility for the accuracy or currency of the Data used in this product or service.\n`;
data += `## Missing stops (${count(osm_missing)})\n`;
for (const ref in osm_missing) {
    const stop = osm_missing[ref];
    data += `* \`${stop.code}\` – ${stop.name}\n`;
}
data += `\n## Existing stops (${count(osm_exists)})\n`;
for (const ref in osm_exists) {
    const stop = osm_exists[ref];
    data += `* \`${stop.code}\` – ${stop.name}\n`;
}
data += `\n## Stops in OSM that TransLink does not list (${count(osm_too_much)})\n`;
for (const ref in osm_too_much) {
    const stop = osm_too_much[ref];
    data += `* \`${ref}\` – ${linkBusStop(stop[0])}\n`;
}
fs.writeFileSync(path.join(repoDir, 'lists/buses/compare_with_translink.md'), data);
