'use strict';
const querystring = require('querystring');
const http = require('http');
const fs = require('fs').promises;

const BBox = require('./helper/bbox.class.js');
const OverpassQuery = require('./helper/overpass-query.class.js');

function askOverpass(codestring) {
    return new Promise((resolve) => {
        // Build the post string from an object
        var post_data = querystring.stringify({
            'data' : codestring,
        });

        // An object of options to indicate where to post to
        var post_options = {
            host: 'overpass-api.de',
            port: '80',
            path: '/api/interpreter',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(post_data),
            }
        };

        // Set up the request
        var post_req = http.request(post_options, function(res) {
            let data = '';
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                data += chunk;
            });
            res.on('end', function (chunk) {
                resolve(data);
            });
        });

        // post the data
        post_req.write(post_data);
        post_req.end();
    });
}

async function writeToCache(name, data) {
    const fileName = `../data/${name}.json`;
    await fs.writeFile(fileName, data, 'utf8');
    return fileName;
}

async function downloadQuery(query, storageName) {
    const result = await askOverpass(query.toString());
    await writeToCache(storageName, result);
}

const vancouverBBox = BBox.fromOverpassString('49.15162235335807,-123.3606719970703,49.392206057422044,-122.84568786621092');
const translinkBBox = BBox.fromOverpassString('49.004,-123.424,49.474,-122.302');

const busQuery = new OverpassQuery();
busQuery.addWish('node', 'highway', 'bus_stop', translinkBBox);
busQuery.addWish('way', 'highway', 'bus_stop', translinkBBox);
busQuery.addWish('relation', 'highway', 'bus_stop', translinkBBox);
downloadQuery(busQuery, 'bus').catch(console.error);

const fireHydrantQuery = new OverpassQuery();
fireHydrantQuery.addWish('node', 'emergency', 'fire_hydrant', vancouverBBox);
downloadQuery(fireHydrantQuery, 'fire_hydrant').catch(console.error);
